// story and different branching paths within
var storyRoot = {
    message: "Welcome to the adventure.",
    isMain: true,
    stories: {
         Begin: {
    		message: "Where to start?",
            isMain: true,
            stories: {
                Foo: {
                    message: "You made it to Foo-land!",
                    stories: {
                        "OK...": {
                            message: "There's actually nothing to do here."
                        }
                    }
                },
                Bar: {
                    message: "You made the right choice coming to Bar-land. Drinks are on me. Lets keep things going...",
                    isMain: true,
                    stories: {
                        "Keep Going": {
                            message: "The end",
                            isMain: true
                        }
                    }
                }
            }
        }
    }	
};

// keeps track of where you are in the story
// including how to get back to main
var storyLocation = { 
    current: null,
    lastMain: null
};

// event listener for handling user selections for story direction
var selectionsEl = document.getElementById("selections");
selectionsEl.addEventListener('click', function (event) {
    var currStory = storyLocation.current;
    var nextStory;
    if (currStory.stories) {
        
        // using the text of the button
        // clicked to lookup the next story
        var label = event.target.textContent;
        nextStory = currStory.stories[label];
        
    } else {
        
        // without a list of sub stories,
        // we go back to the main story
        nextStory = storyLocation.lastMain;
    }
    loadStory(nextStory);
});

function loadStory(currStory) {
    
    // set the current story to the story to load
    storyLocation.current = currStory;
    
    // update message
    var messageEl = document.getElementById("message");
    messageEl.textContent = currStory.message;
    
    // update buttons
    
    // remove old buttons
    var selectionsEl = document.getElementById("selections");
    while (selectionsEl.firstChild) {
        selectionsEl.removeChild(selectionsEl.firstChild);
    }
    
    // add new buttons for each sub story
    var subStory;
    var buttonEl;
    for (var label in currStory.stories) {
        subStory = currStory.stories[label];
        buttonEl = document.createElement('button');
        buttonEl.textContent = label;
        selectionsEl.appendChild(buttonEl);
    }
    
    // update the main story line path, providing
    // a way back button if this is not the main story
    if (currStory.isMain) {
        storyLocation.lastMain = currStory;
    } else if (storyLocation.lastMain && !currStory.stories) {
        buttonEl = document.createElement('button');
        buttonEl.textContent = "Back to main story";
        selectionsEl.appendChild(buttonEl);
    }
}

loadStory(storyRoot);